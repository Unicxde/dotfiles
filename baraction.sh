#!/bin/bash

#:
#:   ▄▀▀█▄▄   ▄▀▀█▄   ▄▀▀▄▀▀▀▄  ▄▀▀█▄   ▄▀▄▄▄▄   ▄▀▀▀█▀▀▄  ▄▀▀█▀▄   ▄▀▀▀▀▄   ▄▀▀▄ ▀▄ 
#:  ▐ ▄▀   █ ▐ ▄▀ ▀▄ █   █   █ ▐ ▄▀ ▀▄ █ █    ▌ █    █  ▐ █   █  █ █      █ █  █ █ █ 
#:    █▄▄▄▀    █▄▄▄█ ▐  █▀▀█▀    █▄▄▄█ ▐ █      ▐   █     ▐   █  ▐ █      █ ▐  █  ▀█ 
#:    █   █   ▄▀   █  ▄▀    █   ▄▀   █   █         █          █    ▀▄    ▄▀   █   █  
#:   ▄▀▄▄▄▀  █   ▄▀  █     █   █   ▄▀   ▄▀▄▄▄▄▀  ▄▀        ▄▀▀▀▀▀▄   ▀▀▀▀   ▄▀   █   
#:  █    ▐   ▐   ▐   ▐     ▐   ▐   ▐   █     ▐  █         █       █         █    ▐   
#:  ▐                                  ▐        ▐         ▐       ▐         ▐        
#:
#:  - Unicxde baraction config
#:  - Sep 23, 2022
#:


###:::: Module Settings


nvidia() {
	echo -n  GTX-1660 - `nvidia-smi -q -d temperature | grep 'GPU Current Temp' | cut -c 45-47 | xargs`°C
}

cpu() {
	echo -n  i7-4790K - `cat /sys/class/thermal/thermal_zone0/temp | sed 's/.\{3\}$/.&/' | rev | cut -c 5-10 | rev`°C
}

ram() {
	echo -n  `free -m | awk 'NR==2{printf "%.2fGB (%.2f%%)\n", $3/1024,$3*100/$2 }'`
}

pkgs() {
	pkgs="$(xbps-query -l | wc -l)"
	echo -e " $pkgs"
}

vol() {
	echo -n  `pulsemixer --get-volume | awk '{print ($1 + $2) / 2}'`
}

###:::: Update Settings


while true; do
	echo "$(vol)  $(pkgs)  $(ram)  $(nvidia)  $(cpu)"
	sleep 8
done
